![Simulator Screen Shot Jan 29, 2017, 6.10.44 PM.png](https://bitbucket.org/repo/LqkGyj/images/3632789379-Simulator%20Screen%20Shot%20Jan%2029,%202017,%206.10.44%20PM.png)
# README #

This app generates gradients based on waves applied to the color components. I don't know why, it looked neat.

Inspired by:
https://mobile.twitter.com/_kzr/status/825327329232629763

Who was inspired by:
http://dev.thi.ng/gradients/

### FEATURES!!!
* You can choose RGB, HSB, or Lab color for the components.
* You can double-tap a slider to directly enter a value.
* Run this on a device with a P3 (extended color gamut) display to see colors no one else can!