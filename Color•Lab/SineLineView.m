
#import "SineLineView.h"

@implementation SineLineView

- (void)drawRect:(CGRect)rect;
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    CGFloat halfHeight = self.bounds.size.height / 2.f;
    CGFloat screenXPosition = 0, screenYPosition = 0;
    
    // Draw gray midline
    [UIColor.lightGrayColor setStroke];
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, 0.f, halfHeight);
    CGContextAddLineToPoint(context, self.bounds.size.width, halfHeight);
    CGContextStrokePath(context);

    // Draw gray gridlines
    for (CGFloat x = halfHeight; x < self.bounds.size.width; x += halfHeight) {
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x, 0.f);
        CGContextAddLineToPoint(context, x, self.bounds.size.height);
        CGContextStrokePath(context);
    }
    
    for (WaveComponents *waveComponents in self.colorWave.allWaveComponents) {
        screenXPosition = 0.f;
        screenYPosition = self.bounds.size.height - (([waveComponents yPositionForXPosition:screenXPosition] * halfHeight) + halfHeight);
        [waveComponents.color setStroke];
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, screenXPosition * self.drawScale, screenYPosition);
        for (CGFloat x = 0.f; x < self.bounds.size.width; x++) {
            screenXPosition = x / self.drawScale;
            screenYPosition = self.bounds.size.height - (([waveComponents yPositionForXPosition:screenXPosition] * halfHeight) + halfHeight);
            CGContextAddLineToPoint(context, screenXPosition * self.drawScale, screenYPosition);
        }
        CGContextStrokePath(context);
    }
}

@end
