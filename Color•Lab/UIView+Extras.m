
#import "UIView+Extras.h"

@implementation UIView (Extras)

- (void)fadeInSubview:(UIView *)view;
{
    view.alpha = 0.f;
    [self addSubview:view];
    [UIView animateWithDuration:0.25 animations:^{ view.alpha = 1.f; }];
}

- (void)makeTapToDismissable;
{
    [self addGestureRecognizer:[UITapGestureRecognizer.alloc initWithTarget:self
                                                                     action:@selector(removeFromSuperview)]];
}

@end
