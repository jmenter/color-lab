
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, WaveType) {
    WaveTypeSine = 0,
    WaveTypeCosine,
    WaveTypeTangent,
    WaveTypeTriangle,
    WaveTypeSawtooth
};

@interface WaveComponents : NSObject

@property (nonatomic) CGFloat frequency;
@property (nonatomic) CGFloat phase;
@property (nonatomic) CGFloat amplitude;
@property (nonatomic) CGFloat bias;

@property (nonatomic) UIColor *color;
@property (nonatomic) WaveType waveType;

+ (instancetype)waveComponentsWithColor:(UIColor *)color;

- (void)setFrequency:(CGFloat)frequency phase:(CGFloat)phase
           amplitude:(CGFloat)amplitude bias:(CGFloat)bias waveType:(WaveType)waveType;
- (CGFloat)normalizedYPositionForXPosition:(CGFloat)xPosition;
- (CGFloat)yPositionForXPosition:(CGFloat)xPosition;

@end
