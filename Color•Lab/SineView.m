
#import "SineView.h"

@implementation SineView

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (!(self = [super initWithFrame:frame])) { return nil; }
    return [self commonInit];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder;
{
    if (!(self = [super initWithCoder:aDecoder])) { return nil; }
    return [self commonInit];
}

- (instancetype)commonInit;
{
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.backgroundColor = UIColor.grayColor;
    return self;
}

- (CGFloat)drawScale;
{
    return self.aspectRatio > 0.f ? self.aspectRatio : self.bounds.size.height / 2.f;
}

@end
