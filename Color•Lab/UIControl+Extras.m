
#import "UIControl+Extras.h"

@implementation UIControl (Extras)

- (void)randomize;
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ srand48(time(0)); });
    
    if ([self isKindOfClass:UISlider.class]) {
        UISlider *selfAsSlider = (UISlider *)self;
        float range = selfAsSlider.maximumValue - selfAsSlider.minimumValue;
        [selfAsSlider setValue:(drand48() * range) + selfAsSlider.minimumValue animated:YES];
    } else if ([self isKindOfClass:UISegmentedControl.class]) {
        UISegmentedControl *selfAsSegmented = (UISegmentedControl *)self;
        selfAsSegmented.selectedSegmentIndex = arc4random_uniform((uint32_t)selfAsSegmented.numberOfSegments);
    }
//    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

@end
