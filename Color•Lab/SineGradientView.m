
#import "SineGradientView.h"

@implementation SineGradientView

- (void)drawRect:(CGRect)rect;
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (CGFloat x = 0.f; x < self.bounds.size.width; x += 1.f / self.contentScaleFactor) {
        [[self.colorWave colorForXPosition:x / self.drawScale] setStroke];
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x, 0.f);
        CGContextAddLineToPoint(context, x, self.bounds.size.height);
        CGContextStrokePath(context);
    }
    
}

@end
