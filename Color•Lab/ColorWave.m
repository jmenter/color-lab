
#import "ColorWave.h"
#import "UIColor+CIELAB.h"

@implementation ColorWave

- (instancetype)init;
{
    self = [self initWithColorMode:ColorModeRGB];
    return self;
}

- (instancetype)initWithColorMode:(ColorMode)colorMode;
{
    if (!(self = [super init])) return nil;
    self.colorMode = colorMode;
    [self assignComponentColors];
    return self;
}

- (void)assignComponentColors;
{
    switch (self.colorMode) {
        case ColorModeRGB:
        default:
            self.primaryWaveComponents = [WaveComponents waveComponentsWithColor:UIColor.redColor];
            self.secondaryWaveComponents = [WaveComponents waveComponentsWithColor:UIColor.greenColor];
            self.tertiaryWaveComponents = [WaveComponents waveComponentsWithColor:UIColor.blueColor];
            break;
        case ColorModeHSV:
            self.primaryWaveComponents = [WaveComponents waveComponentsWithColor:[UIColor colorWithHue:0.f saturation:0.5 brightness:0.75 alpha:1.f]];
            self.secondaryWaveComponents = [WaveComponents waveComponentsWithColor:[UIColor colorWithHue:0.f saturation:1.f brightness:1.f alpha:1.f]];
            self.tertiaryWaveComponents = [WaveComponents waveComponentsWithColor:[UIColor colorWithWhite:0.75 alpha:1.f]];
            break;
        case ColorModeLAB:
            self.primaryWaveComponents = [WaveComponents waveComponentsWithColor:[UIColor colorWithWhite:0.75 alpha:1.f]];
            self.secondaryWaveComponents = [WaveComponents waveComponentsWithColor:[UIColor colorWithLightness:75 A:75 B:0 alpha:1.f]];;
            self.tertiaryWaveComponents = [WaveComponents waveComponentsWithColor:[UIColor colorWithLightness:75 A:0 B:75 alpha:1.f]];
            break;
    }
}

- (NSArray <WaveComponents *> *)allWaveComponents;
{
    return @[self.primaryWaveComponents, self.secondaryWaveComponents, self.tertiaryWaveComponents];
}

- (void)setColorMode:(ColorMode)colorMode;
{
    _colorMode = colorMode;
    [self assignComponentColors];
}

- (UIColor *)colorForXPosition:(CGFloat)xPosition;
{
    UIColor *color;
    switch (self.colorMode) {
        case ColorModeRGB:
        default:
            color = [self rgbColorForXPosition:xPosition];
            break;
        case ColorModeHSV:
            color = [self hsvColorForXPosition:xPosition];
            break;
        case ColorModeLAB:
            color = [self labColorForXPosition:xPosition];
            break;
    }
    return color;
}

- (UIColor *)rgbColorForXPosition:(CGFloat)xPosition;
{
    return [UIColor colorWithRed:[self.primaryWaveComponents normalizedYPositionForXPosition:xPosition]
                           green:[self.secondaryWaveComponents normalizedYPositionForXPosition:xPosition]
                            blue:[self.tertiaryWaveComponents normalizedYPositionForXPosition:xPosition]
                           alpha:1.f];
}

- (UIColor *)hsvColorForXPosition:(CGFloat)xPosition;
{
    return [UIColor colorWithHue:[self.primaryWaveComponents normalizedYPositionForXPosition:xPosition]
                      saturation:[self.secondaryWaveComponents normalizedYPositionForXPosition:xPosition]
                      brightness:[self.tertiaryWaveComponents normalizedYPositionForXPosition:xPosition]
                           alpha:1.f];
}

- (UIColor *)labColorForXPosition:(CGFloat)xPosition;
{
    CGFloat l = [self.primaryWaveComponents normalizedYPositionForXPosition:xPosition];
    CGFloat a = ([self.secondaryWaveComponents normalizedYPositionForXPosition:xPosition] * 2.f) - 1.f;
    CGFloat b = ([self.tertiaryWaveComponents normalizedYPositionForXPosition:xPosition] * 2.f) - 1.f;
    
    l *= 100.f;
    a *= (a > 0.f) ? 98.254173 : 86.184593;
    b *= (b > 0.f) ? 94.482437 : 107.863632;

    return [UIColor colorWithLightness:l A:a B:b alpha:1.f];
}

- (NSString *)minLabelString;
{
    return self.colorMode == ColorModeLAB ? @"-1" : @"0";
}

- (NSString *)midLabelString;
{
    return self.colorMode == ColorModeLAB ? @"0" : @".5";
}

@end
