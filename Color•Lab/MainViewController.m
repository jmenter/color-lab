
#import "MainViewController.h"
#import "SineLineView.h"
#import "SineGradientView.h"
#import "NSArray+UIControlExtras.h"
#import "UIControl+Extras.h"
#import "UIView+Extras.h"

@implementation MainViewController

- (BOOL)prefersStatusBarHidden; { return YES; }

- (void)viewDidLoad; {
    [super viewDidLoad];
    [self colorModeChanged:nil];
}

- (IBAction)randomize;
{
    [self.primarySliders randomize];
    [self.secondarySliders randomize];
    [self.tertiarySliders randomize];
    [self.segmentedControls randomize];
    [self colorModeChanged:nil];
}

- (IBAction)reset;
{
    self.frequencyAmplitueSliders.value = 1.f;
    self.phaseBiasSliders.value = 0.f;
    self.segmentedControls.selectedSegmentIndex = 0;
    [self colorModeChanged:nil];
}

- (IBAction)colorModeChanged:(id)sender;
{
    self.colorWave.colorMode = self.colorModeControl.selectedSegmentIndex;
    [self valueChanged:sender];
    self.primarySliders.thumbTintColor = self.primaryWaveTypeControl.tintColor =
            self.colorWave.primaryWaveComponents.color;
    self.secondarySliders.thumbTintColor = self.secondaryWaveTypeControl.tintColor =
            self.colorWave.secondaryWaveComponents.color;
    self.tertiarySliders.thumbTintColor = self.tertiaryWaveTypeControl.tintColor =
            self.colorWave.tertiaryWaveComponents.color;
    self.labelMin.text = self.colorWave.minLabelString;
    self.labelMid.text = self.colorWave.midLabelString;
}

- (IBAction)valueChanged:(id)sender;
{
    
    [self.colorWave.primaryWaveComponents setFrequency:self.primaryFrequency.value phase:self.primaryPhase.value
                                             amplitude:self.primaryAmplitude.value bias:self.primaryBias.value
                                              waveType:self.primaryWaveTypeControl.selectedSegmentIndex];
    [self.colorWave.secondaryWaveComponents setFrequency:self.secondaryFrequency.value phase:self.secondaryPhase.value
                                               amplitude:self.secondaryAmplitude.value bias:self.secondaryBias.value
                                                waveType:self.secondaryWaveTypeControl.selectedSegmentIndex];
    [self.colorWave.tertiaryWaveComponents setFrequency:self.tertiaryFrequency.value phase:self.tertiaryPhase.value
                                              amplitude:self.tertiaryAmplitude.value bias:self.tertiaryBias.value
                                               waveType:self.tertiaryWaveTypeControl.selectedSegmentIndex];
    [self.sineView setNeedsDisplay];
    [self.gradientView setNeedsDisplay];
}

- (IBAction)viewWasTapped:(UITapGestureRecognizer *)sender;
{
    SineView *view = [sender.view.class.alloc initWithFrame:self.view.bounds];
    view.aspectRatio = 30;
    view.colorWave = self.colorWave;
    [view makeTapToDismissable];
    [self.view fadeInSubview:view];
}

@end
