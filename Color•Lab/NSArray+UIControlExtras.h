
#import <Foundation/Foundation.h>

@interface NSArray (UISliderExtras)

- (void)setThumbTintColor:(UIColor *)color;
- (void)setValue:(float)value;
- (void)randomize;

@end

@interface NSArray (UISegmentedControlExtras)

- (void)setSelectedSegmentIndex:(UISegmentedControlSegment)index;

@end

