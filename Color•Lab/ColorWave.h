
#import <UIKit/UIKit.h>
#import "WaveComponents.h"

typedef NS_ENUM(NSUInteger, ColorMode) {
    ColorModeRGB = 0,
    ColorModeHSV,
    ColorModeLAB,
};

@interface ColorWave : NSObject

@property (nonatomic) ColorMode colorMode;
@property (nonatomic) WaveComponents *primaryWaveComponents;
@property (nonatomic) WaveComponents *secondaryWaveComponents;
@property (nonatomic) WaveComponents *tertiaryWaveComponents;
@property (nonatomic, readonly) NSArray <WaveComponents *> *allWaveComponents;

- (instancetype)initWithColorMode:(ColorMode)colorMode NS_DESIGNATED_INITIALIZER;

- (UIColor *)colorForXPosition:(CGFloat)xPosition;

- (NSString *)minLabelString;
- (NSString *)midLabelString;

@end
