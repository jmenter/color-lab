
#import "WaveComponents.h"

@implementation WaveComponents

+ (instancetype)waveComponentsWithColor:(UIColor *)color;
{
    WaveComponents *waveComponents = WaveComponents.new;
    waveComponents.color = color;
    return waveComponents;
}

- (void)setFrequency:(CGFloat)frequency phase:(CGFloat)phase
           amplitude:(CGFloat)amplitude bias:(CGFloat)bias waveType:(WaveType)waveType;
{
    self.frequency = frequency;
    self.phase = phase;
    self.amplitude = amplitude;
    self.bias = bias;
    self.waveType = waveType;
}

- (CGFloat)normalizedYPositionForXPosition:(CGFloat)xPosition;
{
    return ([self yPositionForXPosition:xPosition] + 1.f) / 2.f;
}

- (CGFloat)yPositionForXPosition:(CGFloat)xPosition;
{
    switch (self.waveType) {
        case WaveTypeSine:
            return [self sineYPositionForXPosition:xPosition];
        case WaveTypeCosine:
            return [self cosineYPositionForXPosition:xPosition];
        case WaveTypeTangent:
            return [self tangentYPositionForXPosition:xPosition];
        case WaveTypeTriangle:
            return [self triangleYPositionForXPosition:xPosition];
        case WaveTypeSawtooth:
            return [self sawtoothYPositionForXPosition:xPosition];
    }
}

- (CGFloat)preMathForXPosition:(CGFloat)xPosition;
{
    return xPosition * (1.f / self.frequency) - self.phase;
}

- (CGFloat)sineYPositionForXPosition:(CGFloat)xPosition;
{
    return [self postMathForXPosition:sin([self preMathForXPosition:xPosition])];
}

- (CGFloat)cosineYPositionForXPosition:(CGFloat)xPosition;
{
    return [self postMathForXPosition:cos([self preMathForXPosition:xPosition])];
}

- (CGFloat)tangentYPositionForXPosition:(CGFloat)xPosition;
{
    return [self postMathForXPosition:tan([self preMathForXPosition:xPosition])];
}

- (CGFloat)triangleYPositionForXPosition:(CGFloat)xPosition;
{
    return [self postMathForXPosition:fabs(fmod([self preMathForXPosition:xPosition], 4.f) - 2.f) - 1.f];
}

- (CGFloat)sawtoothYPositionForXPosition:(CGFloat)xPosition;
{
    return [self postMathForXPosition:fmod([self preMathForXPosition:xPosition], 2.f) - 1.f];
}

- (CGFloat)postMathForXPosition:(CGFloat)xPosition;
{
    return xPosition * self.amplitude + self.bias;
}

@end
