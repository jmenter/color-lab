
#import "LabeledSlider.h"

@interface LabeledSlider ()
@property (nonatomic) UILabel *label;
@property (nonatomic) UILabel *annotationLabel;
@end

@implementation LabeledSlider

- (instancetype)initWithCoder:(NSCoder *)aDecoder;
{
    if (!(self = [super initWithCoder:aDecoder])) return nil;
    
    self.label = UILabel.new;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:17.f];
    self.label.adjustsFontSizeToFitWidth = YES;
    self.label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    self.label.shadowColor = UIColor.blackColor;
    self.label.shadowOffset = CGSizeMake(0.5, 0.5);
    
    [self addSubview:self.label];

    self.annotationLabel = [UILabel.alloc initWithFrame:CGRectMake(2, 0, self.frame.size.width - 2, 11.f)];
    self.annotationLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:9.f];
    self.annotationLabel.textColor = UIColor.lightGrayColor;
    self.annotationLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [self addSubview:self.annotationLabel];
    
    UITapGestureRecognizer *tapGR = [UITapGestureRecognizer.alloc initWithTarget:self action:@selector(controlWasTapped:)];
    tapGR.numberOfTapsRequired = 2;
    [self addGestureRecognizer:tapGR];
    
    return self;
}

- (void)setAnnotationText:(NSString *)annotationText;
{
    self.annotationLabel.text = annotationText;
}

- (NSString *)annotationText;
{
    return self.annotationLabel.text;
}

- (void)controlWasTapped:(UITapGestureRecognizer *)tapGR;
{
    UIResponder *responder = self;
    while (![responder isKindOfClass:UIViewController.class]) {
        responder = responder.nextResponder;
        if (!responder) return;
    }
    UIViewController *viewController = (UIViewController *)responder;
    
    __block UITextField *valueTextField;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:self.annotationText ?: @"Edit Value" message:[NSString stringWithFormat:@"Enter a new value between\n%@ and %@.", [self labelTextForValue:self.minimumValue], [self labelTextForValue:self.maximumValue]] preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        valueTextField = textField;
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        textField.text = [self labelTextForValue:self.value];
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setValue:valueTextField.text.floatValue animated:YES];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }]];
    
    [viewController presentViewController:alertController animated:YES completion:^{
        valueTextField.selectedTextRange = [valueTextField textRangeFromPosition:valueTextField.beginningOfDocument toPosition:valueTextField.endOfDocument];
    }];
}

- (void)layoutSubviews;
{
    [super layoutSubviews];
    [self bringSubviewToFront:self.label];
    self.label.frame = CGRectInset([self thumbRectForBounds:self.bounds trackRect:[self trackRectForBounds:self.bounds] value:self.value], 4.f, 0.f);
    self.label.text = [self labelTextForValue:self.value];
    self.label.textColor = self.labelTextColor;
}

- (NSString *)labelTextForValue:(float)value;
{
    return [NSString stringWithFormat:@"%0.2f", value];
}

- (UIColor *)labelTextColor;
{
    return UIColor.whiteColor;
}

@end
