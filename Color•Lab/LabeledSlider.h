
#import <UIKit/UIKit.h>
#import "JAMAccurateSlider.h"

@interface LabeledSlider : JAMAccurateSlider

@property (nonatomic, copy) IBInspectable NSString *annotationText;

@end
