
#import <UIKit/UIKit.h>

@class SineLineView;
@class SineGradientView;
@class ColorWave;

@interface MainViewController : UIViewController

@property (nonatomic) IBOutlet ColorWave *colorWave;

@property (weak, nonatomic) IBOutlet SineLineView *sineView;
@property (weak, nonatomic) IBOutlet SineGradientView *gradientView;

@property (weak, nonatomic) IBOutlet UILabel *labelMid;
@property (weak, nonatomic) IBOutlet UILabel *labelMin;

@property (weak, nonatomic) IBOutlet UISegmentedControl *colorModeControl;

@property (weak, nonatomic) IBOutlet UISegmentedControl *primaryWaveTypeControl;
@property (weak, nonatomic) IBOutlet UISlider *primaryFrequency;
@property (weak, nonatomic) IBOutlet UISlider *primaryPhase;
@property (weak, nonatomic) IBOutlet UISlider *primaryAmplitude;
@property (weak, nonatomic) IBOutlet UISlider *primaryBias;

@property (weak, nonatomic) IBOutlet UISegmentedControl *secondaryWaveTypeControl;
@property (weak, nonatomic) IBOutlet UISlider *secondaryFrequency;
@property (weak, nonatomic) IBOutlet UISlider *secondaryPhase;
@property (weak, nonatomic) IBOutlet UISlider *secondaryAmplitude;
@property (weak, nonatomic) IBOutlet UISlider *secondaryBias;

@property (weak, nonatomic) IBOutlet UISegmentedControl *tertiaryWaveTypeControl;
@property (weak, nonatomic) IBOutlet UISlider *tertiaryFrequency;
@property (weak, nonatomic) IBOutlet UISlider *tertiaryPhase;
@property (weak, nonatomic) IBOutlet UISlider *tertiaryAmplitude;
@property (weak, nonatomic) IBOutlet UISlider *tertiaryBias;

@property (strong, nonatomic) IBOutletCollection(UISegmentedControl) NSArray <UISegmentedControl *> *segmentedControls;
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray <UISlider *> *primarySliders;
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray <UISlider *> *secondarySliders;
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray <UISlider *> *tertiarySliders;
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray <UISlider *> *frequencyAmplitueSliders;
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray <UISlider *> *phaseBiasSliders;

@end

