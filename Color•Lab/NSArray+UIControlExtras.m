
#import <UIKit/UIKit.h>
#import "NSArray+UIControlExtras.h"
#import "UIControl+Extras.h"

@implementation NSArray (UISliderExtras)

- (void)setThumbTintColor:(UIColor *)color;
{
    for (id object in self) {
        if ([object respondsToSelector:@selector(setThumbTintColor:)]) {
            [object setThumbTintColor:color];
        }
    }
}

- (void)setValue:(float)value;
{
    for (id object in self) {
        if ([object respondsToSelector:@selector(setValue:)]) {
            [(UISlider *)object setValue:value];
        }
    }
}

- (void)randomize;
{
    for (id object in self) {
        if ([object respondsToSelector:@selector(randomize)]) {
            [(UIControl *)object randomize];
        }
    }
}

@end

@implementation NSArray (UISegmentedControlExtras)

- (void)setSelectedSegmentIndex:(UISegmentedControlSegment)index;
{
    for (id object in self) {
        if ([object respondsToSelector:@selector(setSelectedSegmentIndex:)]) {
            [(UISegmentedControl *)object setSelectedSegmentIndex:index];
        }
    }
}

@end
