
#import <UIKit/UIKit.h>
#import "ColorWave.h"

@interface SineView : UIView

@property (nonatomic, weak) IBOutlet ColorWave *colorWave;
@property (nonatomic) CGFloat aspectRatio;

- (CGFloat)drawScale;

@end
