
#import <UIKit/UIKit.h>

@interface UIView (Extras)

- (void)fadeInSubview:(UIView *)view;
- (void)makeTapToDismissable;

@end
